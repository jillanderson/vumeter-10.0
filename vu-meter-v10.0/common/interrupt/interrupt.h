/*******************************************************************************
 *
 * globalne zezwolenie na przerwania
 *
 ******************************************************************************/

#pragma once

#include <avr/builtins.h>
#include <avr/interrupt.h>

/*
 * włącza globalne zezwolenie na przerwania
 */
#if defined (__BUILTIN_AVR_SEI)
static inline __attribute__((always_inline)) void interrupt_global_enable(void)
{
	__builtin_avr_sei();
}
#else
static inline __attribute__((always_inline)) void interrupt_global_enable(void)
{
	sei();
}
#endif

/*
 * wyłącza globalne zezwolenie na przerwania
 */
#if defined (__BUILTIN_AVR_CLI)
static inline __attribute__((always_inline)) void interrupt_global_disable(void)
{
	__builtin_avr_cli();
}
#else
static inline __attribute__((always_inline)) void interrupt_global_disable(void)
{
	cli();
}
#endif

