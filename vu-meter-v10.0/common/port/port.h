/*******************************************************************************
 *
 * manipulowanie portami io
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

static inline __attribute__((always_inline, nonnull)) void port_set_value(
                volatile uint8_t *const port, uint8_t value)
{
	*port = (value);
}

static inline __attribute__((always_inline, nonnull)) void port_set_mask(
                volatile uint8_t *const port, uint8_t mask)
{
	*port |= (mask);
}

static inline __attribute__((always_inline, nonnull)) void port_clear_mask(
                volatile uint8_t *const port, uint8_t mask)
{
	*port &= (uint8_t) (~(mask));
}

static inline __attribute__((always_inline, nonnull)) void port_set_mask_opposite(
                volatile uint8_t *const port, uint8_t mask)
{
	*port ^= (mask);
}

static inline __attribute__((always_inline, nonnull)) uint8_t port_read_value(
                volatile uint8_t *const port)
{
	return (*port);
}

