/*******************************************************************************
 *
 * sterowanie interfejsem jtag
 *
 ******************************************************************************/

#pragma once

#include <avr/io.h>
#include <util/atomic.h>

#if defined (JTD) && defined (MCUCSR)
#define JTAG_REGISTER	MCUCSR
#define JTAG_BIT	JTD
#define JTAG_PRESENT
#endif

/*
 * włącza interfejs jtag
 */
#if defined (JTAG_PRESENT)
static inline __attribute__((always_inline)) void jtag_enable(void)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		JTAG_REGISTER &= (__typeof__(JTAG_REGISTER))(~(1UL << JTAG_BIT));
		JTAG_REGISTER &= (__typeof__(JTAG_REGISTER))(~(1UL << JTAG_BIT));
	}
}
#else
static inline __attribute__((always_inline)) void jtag_enable(void)
{
	;
}
#endif

/*
 * wyłącza interfejs jtag
 */
#if defined (JTAG_PRESENT)
static inline __attribute__((always_inline)) void jtag_disable(void)
{
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
		JTAG_REGISTER |= (1UL << JTAG_BIT);
		JTAG_REGISTER |= (1UL << JTAG_BIT);
	}
}
#else
static inline __attribute__((always_inline)) void jtag_disable(void)
{
	;
}
#endif

