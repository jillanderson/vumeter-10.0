/*******************************************************************************
 *
 * usuwa szybkie zakłócenia sygnału wejściowego
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

typedef enum {
	INTEGRATOR_LEVEL_LOW,
	INTEGRATOR_LEVEL_HIGH
} integrator_level_et;

typedef struct {
	uint8_t steps;
	volatile uint8_t counter;
	volatile integrator_level_et level;
} integrator_st;

/*
 * inicjuje filtr
 *
 * @integrator		wskaźnik do struktury intagratora
 * @steps		ilość kroków integratora
 */
void integrator_init(integrator_st *const integrator, uint8_t steps) __attribute__((nonnull));

/*
 * pobiera poziom filtrowanego sygnału
 *
 * @integrator	wskaźnik do struktury intagratora
 * @ret		poziom przefiltrowanego sygnału
 */
integrator_level_et integrator_level(integrator_st *const integrator) __attribute__((nonnull));

/*
 * uaktualnia stan integratora
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 *
 * @integrator	wskaźnik do struktury intagratora
 * @level	poziom filtrowanego sygnału
 */
void integrator_on_tick(integrator_st *const integrator, integrator_level_et level) __attribute__((nonnull));

