/*******************************************************************************
 *
 * nieblokująca obsługa wbudowanej pamięci EEPROM
 *
 ******************************************************************************/

#pragma once

#include <stddef.h>

typedef enum {
	EE_OK,
	EE_BUSY,
	EE_OVF
} ee_status_et;

typedef void ee_handler_ft(void *arg);

/*
 * funkcja nieblokująca, odczytuje blok n bajtów z adresu src pamięci EEPROM
 * i zapisuje do pamięci SRAM od adresu dst.
 *
 * @src		adres początku bloku EEPROM
 * @dst		adres początku bloku w SRAM
 * @size	ilość bajtów do odczytania
 * @handler	funkcja zwrotna wywoływana po zakończeniu odczytu danych
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 * @ret		EE_OK, inicjalizacja odczytu zakończona sukcesem
 */
ee_status_et ee_read_block(void *restrict dst, void const *restrict src,
                           size_t size, ee_handler_ft *handler, void *arg) __attribute__((nonnull(1,2)));

/*
 * funkcja nieblokująca, zapisuje blok n bajtów z pamięci SRAM od adresu src
 * i zapisuje od adresu dst w pamięci EEPROM
 *
 * @src		wskaźnik początku bloku danych do zapisania
 * @dst		wskaźnik początku bloku w EEPROM
 * @size	ilość bajtów do zapisania
 * @handler	funkcja zwrotna wywoływana po zakończeniu zapisu danych
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 * @ret		EE_OK, inicjalizacja zapisu zakończona sukcesem
 */
ee_status_et ee_write_block(void const *restrict src, void *restrict dst,
                            size_t size, ee_handler_ft *handler, void *arg) __attribute__((nonnull(1,2)));

