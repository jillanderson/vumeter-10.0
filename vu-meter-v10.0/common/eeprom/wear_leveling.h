/*******************************************************************************
 *
 * implementuje technikę "wear leveling EEPROM"
 * wykorzystuje całą dostępną pamięć - adresy 0-E2END
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>
#include <stddef.h>

typedef enum {
	WL_OK,
	WL_BUSY,
	WL_ERROR
} wl_status_et;

/*
 * inicjuje zapis ustawień w pamięci EEPROM
 *
 * @data	bufor odczytu/zapisu danych
 * @size	rozmiar bloku danych
 */
void  wear_leveling_init(void *data, size_t size) __attribute__((nonnull));

/*
 * odczytuje dane z pamięci eeprom do zdefiniowanego bufora
 */
wl_status_et wear_leveling_read(void);

/*
 * zapisuje dane ze zdefiniowanego bufora do pamięci eeprom
 */
wl_status_et wear_leveling_write(void);

/*
 * sprawdza czy odczyt-zapis został zakończony
 */
bool wear_leveling_completed(void) __attribute__((pure));

