#include "../wear_leveling.h"

#include <avr/io.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "../eeprom.h"

enum state {
	WL_UNDEFINED_STATE,
	WL_INITIALIZED_STATE,
	WL_IN_PROGRESS_STATE,
	WL_COMPLETED_STATE
};

static struct {
	struct {
		void *address;
		size_t size;
	} data;
	uint8_t sentinel;
	enum state state;
} wear_leveling = { 0 };

static size_t const SENTINEL_SIZE = sizeof(wear_leveling.sentinel);
static size_t const SENTINEL_MASK = (1UL << (SENTINEL_SIZE * 8U - 1U));

static uintptr_t __find_read_address(void);

static void __init_handler(void *arg);
static void __read_handler(void *arg);
static void __write_handler(void *arg);

void wear_leveling_init(void *data, size_t size)
{
	(wear_leveling.data).address = data;
	(wear_leveling.data).size = size;

	/* ustawia poprawną początkową wartość strażnika */
	uintptr_t read_address = __find_read_address();
	(void) ee_read_block(&(wear_leveling.sentinel),
	                (uintptr_t*) read_address, SENTINEL_SIZE,
	                __init_handler, &(wear_leveling.state));
}

wl_status_et wear_leveling_read(void)
{
	wl_status_et status = WL_OK;

	if (WL_UNDEFINED_STATE == wear_leveling.state) {
		status = WL_ERROR;
	} else if (WL_IN_PROGRESS_STATE == wear_leveling.state) {
		status = WL_BUSY;
	} else {
		wear_leveling.state = WL_IN_PROGRESS_STATE;
		uintptr_t read_address = __find_read_address();
		(void) ee_read_block((wear_leveling.data).address,
		                (uintptr_t*) (read_address + SENTINEL_SIZE),
		                (wear_leveling.data).size, __read_handler,
		                &(wear_leveling.state));
	}

	return (status);
}

wl_status_et wear_leveling_write(void)
{
	wl_status_et status = WL_OK;

	if (WL_UNDEFINED_STATE == wear_leveling.state) {
		status = WL_ERROR;
	} else if (WL_IN_PROGRESS_STATE == wear_leveling.state) {
		status = WL_BUSY;
	} else {
		wear_leveling.state = WL_IN_PROGRESS_STATE;
		size_t const ENTRY_SIZE = (SENTINEL_SIZE
		                + (wear_leveling.data).size);
		uint8_t buffer_tmp[ENTRY_SIZE];
		uintptr_t write_address = (__find_read_address() + ENTRY_SIZE);

		if ((E2END + 1U) < (write_address + ENTRY_SIZE)) {
			write_address = (uintptr_t) ((uint8_t const*) 0U);
			wear_leveling.sentinel ^= SENTINEL_MASK;
		}

		buffer_tmp[0U] = wear_leveling.sentinel;
		memcpy(&buffer_tmp[SENTINEL_SIZE], (wear_leveling.data).address,
		                (wear_leveling.data).size);
		(void) ee_write_block(buffer_tmp, (uintptr_t*) write_address,
		                ENTRY_SIZE, __write_handler,
		                &(wear_leveling.state));
	}

	return (status);
}

bool wear_leveling_completed(void)
{
	return (WL_COMPLETED_STATE == wear_leveling.state ? true : false);
}

/* po resecie pamięci EEPROM wszystkie komórki mają wartości 0xFFU */
uintptr_t __find_read_address(void)
{
	/* umożliwia przekazanie adresu o wartości 0 bez rozwinięcia do NULL */
	uint8_t const *ADDR_0 = 0U;
	size_t const ENTRY_SIZE = (SENTINEL_SIZE + (wear_leveling.data).size);
	typeof(wear_leveling.sentinel) sentinel_tmp;

	(void) ee_read_block(&sentinel_tmp, ADDR_0, SENTINEL_SIZE, NULL, NULL);

	uint8_t sentinel_first = sentinel_tmp & SENTINEL_MASK;

	/* wyszukiwanie binarne */
	uintptr_t left = (uintptr_t) ((uint8_t const*) 0U);
	uintptr_t right = (uintptr_t) ((E2END + 1U) / ENTRY_SIZE);

	while ((left + 1U) != right) {
		uintptr_t mid = left + (right - left) / 2U;

		(void) ee_read_block(&sentinel_tmp,
		                (uintptr_t*) (mid * ENTRY_SIZE), SENTINEL_SIZE,
		                NULL, NULL);
		(sentinel_first != (sentinel_tmp & SENTINEL_MASK)) ?
		                (right = mid) : (left = mid);
	}

	return (left * ENTRY_SIZE);
}

void __init_handler(void *arg)
{
	enum state *state = (enum state*) arg;
	*state = WL_INITIALIZED_STATE;
}

void __read_handler(void *arg)
{
	enum state *state = (enum state*) arg;
	*state = WL_COMPLETED_STATE;
}

void __write_handler(void *arg)
{
	enum state *state = (enum state*) arg;
	*state = WL_COMPLETED_STATE;
}

