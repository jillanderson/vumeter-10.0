#include "../eeprom.h"

#include <avr/eeprom.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

static struct {
	volatile size_t size;
	volatile uint8_t const *src_p;
	volatile uint8_t *dst_p;
	ee_handler_ft *handler;
	void *arg;
} ee_write = { 0 };

/*
 * sprzętowy odczyt wartości komórki EEPROM
 * nie sprawdza gotowości pamięci
 *
 * @addr	adress komórki EEPROM
 */
static inline __attribute__((always_inline))  uint8_t __ee_read_byte(
                uintptr_t addr);

/*
 * sprzętowy zapis podanej wartości do komórki EEPROM
 * nie sprawdza gotowości pamięci
 *
 * @addr	adress komórki EEPROM
 * @data	wartość do zapisania
 */
static inline __attribute__((always_inline)) void __ee_write_byte(
                uintptr_t addr, uint8_t data);

/*
 * włącza przerwanie EE_RDY_vect
 */
static inline __attribute__((always_inline)) void __ee_isr_enable(void);

/*
 * wyłącza przerwanie EE_RDY_vect
 */
static inline __attribute__((always_inline)) void __ee_isr_disable(void);

/*
 * testuje czy pamięć jest zajęta
 */
static inline __attribute__((always_inline)) bool __ee_is_busy(void);

ee_status_et ee_read_block(void *restrict dst, void const *restrict src,
                           size_t size, ee_handler_ft *handler, void *arg)
{
	ee_status_et status = EE_OK;

	if ((E2END + 1U) < ((uintptr_t) src + size)) {
		status = EE_OVF;
	} else if (true == __ee_is_busy()) {
		status = EE_BUSY;
	} else {
		uint8_t *dst_p = dst;
		uint8_t const *src_p = src;

		while (0U < size--) {
			*(dst_p++) = __ee_read_byte((uintptr_t) (src_p++));
		}

		if (NULL != handler) {
			handler(arg);
		}
	}

	return (status);
}

ee_status_et ee_write_block(void const *restrict src, void *restrict dst,
                            size_t size, ee_handler_ft *handler, void *arg)
{
	ee_status_et status = EE_OK;

	if ((E2END + 1U) < ((uintptr_t) dst + size)) {
		status = EE_OVF;
	} else if (true == __ee_is_busy()) {
		status = EE_BUSY;
	} else {
		ee_write.handler = handler;
		ee_write.arg = arg;
		ee_write.size = size;
		ee_write.dst_p = dst;
		ee_write.src_p = src;

		__ee_isr_enable();
	}

	return (status);
}

ISR(EE_RDY_vect)
{
	if (0U < ee_write.size--) {
		uint8_t src_data = *(ee_write.src_p++);
		uint8_t ee_data = __ee_read_byte((uintptr_t) (ee_write.dst_p));

		if (ee_data != src_data) {
			__ee_write_byte((uintptr_t) (ee_write.dst_p), src_data);
		}

		ee_write.dst_p++;
	} else {
		__ee_isr_disable();

		if (NULL != ee_write.handler) {
			(ee_write.handler)(ee_write.arg);
		}
	}
}

uint8_t __ee_read_byte(uintptr_t addr)
{
	EEAR = addr;
	EECR |= (1UL << EERE);

	return (EEDR);
}

void __ee_write_byte(uintptr_t addr, uint8_t data)
{
	EEAR = addr;
	EEDR = data;
	EECR |= (1UL << EEMWE);
	EECR |= (1UL << EEWE);
}

void __ee_isr_enable(void)
{
	EECR |= (1UL << EERIE);
}

void __ee_isr_disable(void)
{
	EECR &= (uint8_t) (~(1UL << EERIE));
}

bool __ee_is_busy(void)
{
	return ((0U == (EECR & (1UL << EEWE))) ? false : true);
}

