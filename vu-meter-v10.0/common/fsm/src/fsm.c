#include "../fsm.h"

static fsm_msg_st const START_MSG = (fsm_msg_st ) { .event = FSM_START_EV };
static fsm_msg_st const ENTRY_MSG = (fsm_msg_st ) { .event = FSM_ENTRY_EV };
static fsm_msg_st const EXIT_MSG = (fsm_msg_st ) { .event = FSM_EXIT_EV };

void fsm_state_ctor(fsm_state_st *const state,
                    fsm_event_handler_ft *const handler)
{
	state->handler = handler;
}

void fsm_ctor(fsm_st *const me, fsm_state_st *const state)
{
	me->current = state;
}

void fsm_on_start(fsm_st *const me)
{
	(*(me->current)->handler)(me, &ENTRY_MSG);
	(*(me->current)->handler)(me, &START_MSG);
}

void fsm_on_event(fsm_st *const me, fsm_msg_st const *const msg)
{
	(*(me->current)->handler)(me, msg);
}

void fsm_state_transition(fsm_st *const me, fsm_state_st *const target)
{
	(*(me->current)->handler)(me, &EXIT_MSG);
	me->current = target;
	(*(me->current)->handler)(me, &ENTRY_MSG);
}

