/*******************************************************************************
 *
 * suma kontrolna crc8 obliczana wg Dallas Semiconductor
 *
 * polynomial X^8 + X^5 + X^4 + X^0
 *
 ******************************************************************************/

#pragma once

#include <stddef.h>
#include <stdint.h>

/*
 * oblicza sumę kontrolną crc8
 *
 * crc		poprzednia wartość CRC
 * data		wskaźnik do bufora z danymi
 * size		rozmiar bufora
 *
 * zwraca uaktualnioną wartość CRC
 */
uint8_t crc8_checksum(uint8_t crc, uint8_t const *data, size_t size);

