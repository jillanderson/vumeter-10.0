/*******************************************************************************
 *
 * standardowa suma kontrolna crc16
 *
 *   Poly  0x8005 (x^16 + x^15 + x^2 + 1)
 *   Init  0
 *
 ******************************************************************************/

#pragma once

#include <stddef.h>
#include <stdint.h>

/*
 * oblicza sumę kontrolną crc16
 *
 * crc		poprzednia wartość CRC
 * data		wskaźnik do bufora z danymi
 * size		rozmiar bufora
 *
 * zwraca uaktualnioną wartość CRC
 */
uint16_t crc16_checksum(uint16_t crc, uint8_t const *data, size_t size);

