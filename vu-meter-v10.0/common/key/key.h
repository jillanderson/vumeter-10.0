/*******************************************************************************
 *
 * wykrywa rodzaj wciśnięcia przycisku
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdint.h>

typedef enum {
	KEY_NOT_PRESSED_EVENT,
	KEY_SHORT_PRESSED_EVENT,
	KEY_DOUBLE_PRESSED_EVENT,
	KEY_LONG_PRESS_START_EVENT,
	KEY_LONG_PRESSED_EVENT,
	KEY_LONG_PRESS_END_EVENT
} key_event_et;

typedef void key_handler_ft(key_event_et event, void *arg);

typedef struct {
	uint16_t double_press_gap_time;
	uint16_t long_press_start_time;
	uint16_t long_press_repeat_time;
	volatile uint16_t counter;
	volatile uint8_t state;
	volatile key_event_et event;
	key_handler_ft *handler;
	void *arg;
} key_st;

/*
 * inicjuje detektor zdarzeń
 *
 * @key				wskaźnik przycisku
 * @double_press_gap_time	czas oczekiwania na dwuklik
 * @long_press_start_time	czas wykrycia długiego wciśnięcia
 * @long_press_repeat_time	czas repetycji przy długim wciśnięciu
 */
void key_init(key_st *const key, uint16_t double_press_gap_time,
              uint16_t long_press_start_time, uint16_t long_press_repeat_time) __attribute__((nonnull));

/*
 * rejestruje funkcję zwrotną
 *
 * @key		wskaźnik przycisku
 * @handler	funkcja zwrotna obsługująca wykryte zdarzenia
 * @arg		wskaźnik do argumentu funkcji zwrotnej
 */
void key_handler(key_st *const key, key_handler_ft *handler, void *arg) __attribute__((nonnull(1)));

/*
 * przetwarza zarejestrowane zadania
 * musi być wywoływana w pętli głównej programu
 *
 * @key		wskaźnik przycisku
 */
void key_dispatch(key_st *const key) __attribute__((nonnull));

/*
 * uaktualnia stan detektora
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 *
 * @detector		wskaźnik do detektora
 * @key_is_pressed	sprzętowy stan przycisku
 */
void key_on_tick(key_st *const key, bool pressed) __attribute__((nonnull));

