/*******************************************************************************
 *
 * timer programowy
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef void timer_handler_ft(void *arg);

typedef struct {
	volatile uint16_t counter;
	timer_handler_ft *handler;
	void *arg;
	volatile enum {
		TIMER_STOPPED,
		TIMER_RUNNING,
		TIMER_EXPIRED
	} status;
} timer_st;

/*
 * inicjuje timer
 *
 * @timer	wskaźnik do struktury timera
 */
void timer_init(timer_st *const timer) __attribute__((nonnull));

/*
 * rejestruje handler wykonywany po zakończeniu odliczania
 *
 * @timer	wskaźnik do struktury timera
 * @handler	funkcja zwrotna wykonywana po zakończeniu odliczania
 * @arg		argument funkcji zwrotnej
 */
void timer_handler(timer_st *const timer, timer_handler_ft *handler, void *arg) __attribute__((nonnull(1,2)));

/*
 * uruchamia timer
 *
 * @timer	wskaźnik do struktury timera
 * @time	wartość do odliczenia
 */
void timer_start(timer_st *const timer, uint16_t time) __attribute__((nonnull));

/*
 * zatrzymuje timer
 *
 * @timer	wskaźnik do struktury timera
 */
void timer_stop(timer_st *const timer) __attribute__((nonnull));

/*
 * testuje czy timer jest zatrzymany
 *
 * @timer	wskaźnik do struktury timera
 */
bool timer_is_stopped(timer_st *const timer);

/*
 * przetwarza zarejestrowany handler
 *
 * @timer	wskaźnik do struktury timera
 */
void timer_dispatch(timer_st *const timer) __attribute__((nonnull));

/*
 * odlicza jednostki czasu
 *
 * funkcja powinna być wywoływana cyklicznie okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu dla timera
 *
 * @timer	wskaźnik do struktury timera
 */
void timer_on_tick(timer_st *const timer) __attribute__((nonnull));

