#include "../timer.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void timer_init(timer_st *const timer)
{
	timer->counter = 0U;
	timer->handler = NULL;
	timer->arg = NULL;
	timer->status = TIMER_STOPPED;
}

void timer_handler(timer_st *const timer, timer_handler_ft *handler, void *arg)
{
	timer->handler = handler;
	timer->arg = arg;
}

void timer_start(timer_st *const timer, uint16_t time)
{
	timer->counter = time;
	timer->status = TIMER_RUNNING;
}

void timer_stop(timer_st *const timer)
{
	timer->status = TIMER_STOPPED;
}

bool timer_is_stopped(timer_st *const timer)
{
	return ((TIMER_STOPPED == timer->status) ? true : false);
}

void timer_dispatch(timer_st *const timer)
{
	if (TIMER_EXPIRED == timer->status) {
		timer->status = TIMER_STOPPED;
		if (NULL != timer->handler) {
			(timer->handler)(timer->arg);
		}
	}
}

void timer_on_tick(timer_st *const timer)
{
	if (TIMER_RUNNING == timer->status) {
		if (0U < timer->counter) {
			timer->counter--;
		} else {
			timer->status = TIMER_EXPIRED;
		}
	}
}

