#include "../device.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "../../common/buffer/ring_buffer.h"
#include "../../common/crc/crc16.h"
#include "../../common/eeprom/wear_leveling.h"
#include "../../common/fsm/fsm.h"
#include "../../common/interrupt/interrupt.h"
#include "../../common/key/key.h"
#include "../../common/timer/timer.h"
#include "../../drv/backlight.h"
#include "../../drv/control.h"
#include "../../drv/indicator.h"
#include "../../drv/mcu.h"
#include "../../drv/timestamp.h"
#include "../../drv/vumeter.h"

/* konfiguracja stałych czasowych */
static uint16_t const device_startup_delay_ms = 2500U;

static uint16_t const indicator_blink_on_time_ms = 800U;
static uint16_t const indicator_blink_off_time_ms = 200U;

static uint8_t const key_debounce_time_ms = 50U;
static uint16_t const key_double_press_gap_time_ms = 600U;
static uint16_t const key_long_press_start_time_ms = 1000U;
static uint16_t const key_long_press_repeat_time_ms = 250U;

/* musi być potęgą 2^n */
enum {
	EVENT_QUEUE_SIZE = 2U
};

enum device_event {
	DEVICE_NOTHING_EV = FSM_FIRST_USER_EV,
	DEVICE_START_EV,
	DEVICE_POWER_EV,
	DEVICE_BACKLIGHT_INCREASE_EV,
	DEVICE_BACKLIGHT_DECREASE_EV
};

typedef struct {
	backlight_et backlight;
} setting_st;

static struct {
	fsm_st fsm;
	fsm_state_st initial;
	fsm_state_st standby;
	fsm_state_st working;
	ring_buffer_st event_queue;
	uint8_t queue_container[EVENT_QUEUE_SIZE];
	setting_st setting;
	timer_st wait;
} device = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

static struct {
	setting_st setting;
	uint16_t crc;
} eemem_store = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

static setting_st const __flash SETTING_DEFAULT =
                { .backlight = BACKLIGHT_NONE };

static void initial_state_handler(fsm_st *me, fsm_msg_st const *msg);
static void standby_state_handler(fsm_st *me, fsm_msg_st const *msg);
static void working_state_handler(fsm_st *me, fsm_msg_st const *msg);

static void message_init(void);
static void message_send_event(uint8_t event);
static uint8_t message_read_event(void);

static void control_event_handler(key_event_et event, void *arg);
static void wait_event_handler(void *arg);
static void timestamp_event_handler(void *arg);

static void setting_init(void);
static bool setting_is_changed(void);
static void setting_restore(void);
static void setting_store(void);

void device_init(void)
{
	mcu_init();
	vumeter_init();
	indicator_init();
	control_init(key_debounce_time_ms, key_double_press_gap_time_ms,
	                key_long_press_start_time_ms,
	                key_long_press_repeat_time_ms);
	control_handler(control_event_handler, NULL);
	backlight_init();
	message_init();
	setting_init();
	setting_restore();
	timer_init(&(device.wait));
	timer_handler(&(device.wait), wait_event_handler, NULL);
	timestamp_start(timestamp_event_handler, NULL);
	fsm_state_ctor(&(device.initial), initial_state_handler);
	fsm_state_ctor(&(device.standby), standby_state_handler);
	fsm_state_ctor(&(device.working), working_state_handler);
	fsm_ctor(&(device.fsm), &(device.initial));
	fsm_on_start(&(device.fsm));
	interrupt_global_enable();
}

void device_process(void)
{
	control_dispatch();
	indicator_dispatch();
	vumeter_dispatch();
	timer_dispatch(&(device.wait));

	uint8_t event = message_read_event();

	if (DEVICE_NOTHING_EV != event) {
		fsm_on_event(&(device.fsm), &((fsm_msg_st ) { .event = event } ));
	}
}

void initial_state_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_START_EV:
		timer_start(&(device.wait), device_startup_delay_ms);
		break;

	case DEVICE_START_EV:
		fsm_state_transition(me, &(device.standby));
		break;

	default:
		;
		break;
	}
}

void standby_state_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_ENTRY_EV:
		indicator_blink(indicator_blink_on_time_ms,
		                indicator_blink_off_time_ms);
		backlight_level(BACKLIGHT_NONE);
		vumeter_off();
		break;

	case DEVICE_POWER_EV:
		fsm_state_transition(me, &(device.working));
		break;

	default:
		;
		break;
	}
}

void working_state_handler(fsm_st *me, fsm_msg_st const *msg)
{
	switch (msg->event) {
	case FSM_ENTRY_EV:
		indicator_on();
		backlight_level((device.setting).backlight);
		vumeter_on();
		break;

	case FSM_EXIT_EV:
		if (true == setting_is_changed()) {
			setting_store();
		}
		break;

	case DEVICE_POWER_EV:
		fsm_state_transition(me, &(device.standby));
		break;

	case DEVICE_BACKLIGHT_INCREASE_EV:
		if (BACKLIGHT_MAXIMUM > (device.setting).backlight) {
			(device.setting).backlight =
			                (uint8_t) ((device.setting).backlight + 1U);
		} else {
			(device.setting).backlight = BACKLIGHT_NONE;
		}

		backlight_level((device.setting).backlight);
		break;

	case DEVICE_BACKLIGHT_DECREASE_EV:
		if (BACKLIGHT_NONE < (device.setting).backlight) {
			(device.setting).backlight =
			                (uint8_t) ((device.setting).backlight - 1U);
		} else {
			(device.setting).backlight = BACKLIGHT_MAXIMUM;
		}

		backlight_level((device.setting).backlight);
		break;

	default:
		;
		break;
	}
}

void message_init(void)
{
	ring_buffer_init(&(device.event_queue), device.queue_container, EVENT_QUEUE_SIZE);
}

void message_send_event(uint8_t event)
{
	ring_buffer_reject_put(&(device.event_queue), event);
}

uint8_t message_read_event(void)
{
	uint8_t event = DEVICE_NOTHING_EV;

	ring_buffer_get(&(device.event_queue), &event);

	return (event);
}

void control_event_handler(key_event_et event, __attribute__((unused)) void *arg)
{
	switch (event) {
	case KEY_SHORT_PRESSED_EVENT:
		message_send_event(DEVICE_POWER_EV);
		break;

	case KEY_DOUBLE_PRESSED_EVENT:
		message_send_event(DEVICE_BACKLIGHT_INCREASE_EV);
		break;

	case KEY_LONG_PRESS_START_EVENT:
		message_send_event(DEVICE_BACKLIGHT_DECREASE_EV);
		break;

	default:
		;
		break;
	}
}

void wait_event_handler(__attribute__((unused)) void *arg)
{
	message_send_event(DEVICE_START_EV);
}

void timestamp_event_handler(__attribute__((unused)) void *arg)
{
	timer_on_tick(&(device.wait));
	control_on_tick();
	vumeter_on_tick();
	indicator_on_tick();
}

void setting_init(void)
{
	device.setting = SETTING_DEFAULT;
	wear_leveling_init(&eemem_store, sizeof(eemem_store));
}

bool setting_is_changed(void)
{
	bool changed = false;

	if ((eemem_store.setting).backlight != (device.setting).backlight) {
		changed = true;
	}

	return (changed);
}

void setting_restore(void)
{
	wear_leveling_read();

	static uint16_t const crc_initial = 0U;

	uint16_t const crc = crc16_checksum(crc_initial,
	                (uint8_t const*) &(eemem_store.setting),
	                sizeof(eemem_store.setting));

	if (crc == eemem_store.crc) {
		device.setting = eemem_store.setting;
	}
}

void setting_store(void)
{
	static uint16_t const crc_initial = 0U;

	eemem_store.crc = crc16_checksum(crc_initial,
	                (uint8_t const*) &(device.setting),
	                sizeof(device.setting));
	eemem_store.setting = device.setting;
	wear_leveling_write();
}

