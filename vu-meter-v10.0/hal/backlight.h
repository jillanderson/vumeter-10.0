/*******************************************************************************
 *
 * obsługa podświetlenia wskaźników VU
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja - podświetlenie wyłączone
 */
void hal_backlight_init(void);

/*
 * włącza podświetlenie
 */
void hal_backlight_on(void);

/*
 * wyłącza podświetlenie
 */
void hal_backlight_off(void);

/*
 * przełącza podświetlenie w stan przeciwny
 */
void hal_backlight_toggle(void);

/*
 * sprawdza czy podświetlenie jest włączone
 */
bool hal_backlight_is_on(void);

