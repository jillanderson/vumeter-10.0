/*******************************************************************************
 *
 * obsługa przycisku sterującego
 * wciśnięcie przycisku zwiera pin do masy
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja przycisku
 */
void hal_key_init(void);

/*
 * testuje czy przycisk jest wciśnięty
 */
bool hal_key_is_pressed(void);

