/*******************************************************************************
 *
 * obsługa wskaźnika optycznego w przycisku sterującym
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja - wskaźnik wyłączony
 */
void hal_indicator_init(void);

/*
 * włącza wskaźnik
 */
void hal_indicator_on(void);

/*
 * wyłącza wskaźnik
 */
void hal_indicator_off(void);

/*
 * przełącza wskaźnik w stan przeciwny
 */
void hal_indicator_toggle(void);

/*
 * sprawdza czy wskaźnik jest włączony
 */
bool hal_indicator_is_on(void);

