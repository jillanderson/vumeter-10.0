/*******************************************************************************
 *
 * Sterowanie 8 bitowym timerem nr 2 w trybie CTC
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

typedef enum {
	TIMER2_PRESCALER_0,
	TIMER2_PRESCALER_1,
	TIMER2_PRESCALER_8,
	TIMER2_PRESCALER_32,
	TIMER2_PRESCALER_64,
	TIMER2_PRESCALER_128,
	TIMER2_PRESCALER_256,
	TIMER2_PRESCALER_1024
} timer2_prescaler_et;

typedef void timer2_handler_ft(void *arg);

/*
 * uruchamia timer w trybie CTC, włącza przerwanie TIMER2_COMP_vect
 */
void hal_mcu_timer2_start(timer2_prescaler_et prescaler, uint8_t top);

/*
 * zatrzymuje timer, wyłącza przerwania
 */
void hal_mcu_timer2_stop(void);

/*
 * rejestruje handler wykonywany w przerwaniu TIMER2_COMP_vect
 */
void hal_mcu_timer2_handler(timer2_handler_ft *handler, void *arg);

