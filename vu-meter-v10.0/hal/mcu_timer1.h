/*******************************************************************************
 *
 * Sterowanie 16 bitowym timerem nr 1 w trybie CTC
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

typedef enum {
	TIMER1_PRESCALER_0,
	TIMER1_PRESCALER_1,
	TIMER1_PRESCALER_8,
	TIMER1_PRESCALER_64,
	TIMER1_PRESCALER_256,
	TIMER1_PRESCALER_1024
} timer1_prescaler_et;

typedef void timer1_handler_ft(void *arg);

/*
 * uruchamia timer w trybie CTC, włącza przerwania TIMER1_COMPA_vect i TIMER1_COMPB_vect
 */
void hal_mcu_timer1_start(timer1_prescaler_et prescaler, uint16_t top, uint16_t compare);

/*
 * zatrzymuje timer, wyłącza przerwania
 */
void hal_mcu_timer1_stop(void);

/*
 * rejestruje handler wykonywany w przerwaniu TIMER1_COMPA_vect
 */
void hal_mcu_timer1_handler_cmpa(timer1_handler_ft *handler, void *arg);

/*
 * rejestruje handler wykonywany w przerwaniu TIMER1_COMPB_vect
 */
void hal_mcu_timer1_handler_cmpb(timer1_handler_ft *handler, void *arg);

