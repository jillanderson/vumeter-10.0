#include "../mcu_timer1.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

static struct {
	timer1_handler_ft *handler_cmpa;
	timer1_handler_ft *handler_cmpb;
	void *arg_cmpa;
	void *arg_cmpb;
} timer = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

void hal_mcu_timer1_start(timer1_prescaler_et prescaler, uint16_t top,
                      uint16_t compare)
{
	TCCR1A = 0U;
	TCCR1B = (uint8_t) ((1U << WGM12) + prescaler);
	TIFR |= (1U << OCF1A); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIFR |= (1U << OCF1B); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK |= (1U << OCIE1A);
	TIMSK |= (1U << OCIE1B);
	OCR1A = top;
	OCR1B = compare;
	TCNT1 = 0U;
}

void hal_mcu_timer1_stop(void)
{
	TCCR1A = 0U;
	TCCR1B = 0U;
	TIFR |= (1U << OCF1A); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIFR |= (1U << OCF1B); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK &= (uint8_t) (~(1U << OCIE1A));
	TIMSK &= (uint8_t) (~(1U << OCIE1B));
}

void hal_mcu_timer1_handler_cmpa(timer1_handler_ft *handler, void *arg)
{
	timer.handler_cmpa = handler;
	timer.arg_cmpa = arg;
}

void hal_mcu_timer1_handler_cmpb(timer1_handler_ft *handler, void *arg)
{
	timer.handler_cmpb = handler;
	timer.arg_cmpb = arg;
}

ISR(TIMER1_COMPA_vect)
{
	if (NULL != timer.handler_cmpa) {
		(timer.handler_cmpa)(timer.arg_cmpa);
	}
}

ISR(TIMER1_COMPB_vect)
{
	if (NULL != timer.handler_cmpb) {
		(timer.handler_cmpb)(timer.arg_cmpb);
	}
}

