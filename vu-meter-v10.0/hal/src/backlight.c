#include "../backlight.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_backlight_left = { .port = &PORTC, .pin = PC3 };
static io_st const io_backlight_right = { .port = &PORTA, .pin = PA0 };

void hal_backlight_init(void)
{
	io_set_low(&io_backlight_left);
	io_set_as_output(&io_backlight_left);
	io_set_low(&io_backlight_right);
	io_set_as_output(&io_backlight_right);
}

void hal_backlight_on(void)
{
	io_set_high(&io_backlight_left);
	io_set_high(&io_backlight_right);
}

void hal_backlight_off(void)
{
	io_set_low(&io_backlight_left);
	io_set_low(&io_backlight_right);
}

void hal_backlight_toggle(void)
{
	io_set_opposite(&io_backlight_left);
	io_set_opposite(&io_backlight_right);
}

bool hal_backlight_is_on(void)
{
	return (((0U == io_read_pin(&io_backlight_left))
	                || (0U == io_read_pin(&io_backlight_right))) ?
	                false : true);
}

