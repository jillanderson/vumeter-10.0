#include "../meter.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_meter = { .port = &PORTC, .pin = PC7 };

void hal_meter_init(void)
{
	io_set_low(&io_meter);
	io_set_as_output(&io_meter);
}

void hal_meter_on(void)
{
	io_set_high(&io_meter);
}

void hal_meter_off(void)
{
	io_set_low(&io_meter);
}

void hal_meter_toggle(void)
{
	io_set_opposite(&io_meter);
}

bool hal_meter_is_on(void)
{
	return ((0U == io_read_pin(&io_meter)) ? false : true);
}

