#include "../input.h"

#include <avr/io.h>
#include <stdbool.h>

#include "../../common/io/io.h"

static io_st const io_input_0 = { .port = &PORTA, .pin = PA4 };
static io_st const io_input_1 = { .port = &PORTA, .pin = PA5 };

void hal_input_init(void)
{
	io_set_low(&io_input_0);
	io_set_as_output(&io_input_0);
	io_set_low(&io_input_1);
	io_set_as_output(&io_input_1);
}

void hal_input_on(void)
{
	io_set_high(&io_input_0);
	io_set_high(&io_input_1);
}

void hal_input_off(void)
{
	io_set_low(&io_input_0);
	io_set_low(&io_input_1);
}

void hal_input_toggle(void)
{
	io_set_opposite(&io_input_0);
	io_set_opposite(&io_input_1);
}

bool hal_input_is_on(void)
{
	return (((0U == io_read_pin(&io_input_0))
	                || (0U == io_read_pin(&io_input_1))) ? false : true);
}

