#include "../mcu_timer2.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <stddef.h>
#include <stdint.h>

static struct {
	timer2_handler_ft *handler;
	void *arg;
} timer = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

void hal_mcu_timer2_start(timer2_prescaler_et prescaler, uint8_t top)
{
	TCCR2 = (uint8_t) ((1UL << WGM21) + prescaler);
	TIFR |= (1UL << OCF2); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK |= (1UL << OCIE2);
	OCR2 = top;
	TCNT2 = 0U;
}

void hal_mcu_timer2_stop(void)
{
	TCCR2 = 0U;
	TIFR |= (1UL << OCF2); /* flagę przerwania kasujemy przez wpisanie jedynki */
	TIMSK &= (uint8_t) (~(1UL << OCIE2));
}

void hal_mcu_timer2_handler(timer2_handler_ft *handler, void *arg)
{
	timer.handler = handler;
	timer.arg = arg;
}

ISR(TIMER2_COMP_vect)
{
	if (NULL != timer.handler) {
		(timer.handler)(timer.arg);
	}
}

