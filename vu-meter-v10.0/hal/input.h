/*******************************************************************************
 *
 * obsługa sterowania wejściami audio
 *
 ******************************************************************************/

#pragma once

#include <stdbool.h>

/*
 * inicjalizacja - wejścia audio odłączone
 */
void hal_input_init(void);

/*
 * włącza wejścia audio
 */
void hal_input_on(void);

/*
 * wyłącza wejścia audio
 */
void hal_input_off(void);

/*
 * przełącza wejścia audio w stan przeciwny
 */
void hal_input_toggle(void);

/*
 * sprawdza czy wejścia audio są włączone
 */
bool hal_input_is_on(void);

