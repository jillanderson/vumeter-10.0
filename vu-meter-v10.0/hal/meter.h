/*******************************************************************************
 *
 * obsługa wejścia miernika
 *
 ******************************************************************************/

#include <stdbool.h>

/*
 * inicjalizacja - wejście miernika wyłączone
 */
void hal_meter_init(void);

/*
 * włącza wejście
 */
void hal_meter_on(void);

/*
 * wyłącza wejście
 */
void hal_meter_off(void);

/*
 * przełącza wejście w stan przeciwny
 */
void hal_meter_toggle(void);

/*
 * sprawdza czy wejście jest włączone
 */
bool hal_meter_is_on(void);

