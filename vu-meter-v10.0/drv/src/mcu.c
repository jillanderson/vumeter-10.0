#include "../mcu.h"

#include "../../common/jtag/jtag.h"
#include "../../hal/mcu_gpio.h"

void mcu_init(void)
{
	jtag_disable();
	hal_mcu_gpio_init();
}

