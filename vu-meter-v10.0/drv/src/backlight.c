#include "../../hal/backlight.h"
#include "../backlight.h"

#include <stddef.h>
#include <stdint.h>

#include "../../hal/mcu_timer1.h"

/*
 * Tabela odwzorowuje liniowe wartości wejściowe (liczby, których chcielibyśmy użyć;
 * np. 127 = połowa jasności) do nieliniowych wartości wyjściowych z korekcją gamma
 * (liczby dające pożądany efekt na diodzie LED; np. 36 = połowa jasności).
 */
static uint8_t const __flash gamma_8[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3,
                3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8,
                8, 9, 9, 9, 10, 10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14,
                15, 15, 16, 16, 17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22,
                23, 24, 24, 25, 25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33,
                34, 35, 35, 36, 37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47,
                48, 49, 50, 50, 51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63,
                64, 66, 67, 68, 69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83,
                85, 86, 87, 89, 90, 92, 93, 95, 96, 98, 99, 101, 102, 104, 105,
                107, 109, 110, 112, 114, 115, 117, 119, 120, 122, 124, 126, 127,
                129, 131, 133, 135, 137, 138, 140, 142, 144, 146, 148, 150, 152,
                154, 156, 158, 160, 162, 164, 167, 169, 171, 173, 175, 177, 180,
                182, 184, 186, 189, 191, 193, 196, 198, 200, 203, 205, 208, 210,
                213, 215, 218, 220, 223, 225, 228, 231, 233, 236, 239, 241, 244,
                247, 249, 252, 255 };

static void handler_cmpa(void *arg);
static void handler_cmpb(void *arg);

void backlight_init(void)
{
	hal_backlight_init();
	hal_mcu_timer1_handler_cmpa(handler_cmpa, NULL);
	hal_mcu_timer1_handler_cmpb(handler_cmpb, NULL);
}

void backlight_level(backlight_et level)
{
	/* konfiguruje częstotliwość odświerzania 100Hz */
	static uint16_t const t1_top = 9999U;
	/* (255 / 51) = 5, [0-51-102-153-204-255] */
	static uint8_t const backlight_step = 51U;

	hal_mcu_timer1_stop();

	switch (level) {
	case BACKLIGHT_NONE:
		hal_backlight_off();
		break;

	case (BACKLIGHT_NONE + 1U) ... (BACKLIGHT_MAXIMUM - 1U):
		hal_mcu_timer1_start(TIMER1_PRESCALER_8, t1_top,
		                (uint16_t) (((uint32_t) t1_top
		                * gamma_8[(level * backlight_step)]) / UINT8_MAX));
		break;

	case BACKLIGHT_MAXIMUM:
		hal_backlight_on();
		break;

	default:
		;
		break;
	}
}

void handler_cmpa(__attribute__((unused)) void *arg)
{
	hal_backlight_on();
}

void handler_cmpb(__attribute__((unused)) void *arg)
{
	hal_backlight_off();
}

