#include "../../hal/indicator.h"
#include "../indicator.h"

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "../../common/timer/timer.h"

static struct {
	uint16_t on;
	uint16_t off;
	timer_st duration;
} indicator = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

static void state_toggle(void);
static void duration_handler(void *arg);

void indicator_init(void)
{
	hal_indicator_init();
	timer_init(&(indicator.duration));
	timer_handler(&(indicator.duration), duration_handler, NULL);
}

void indicator_on(void)
{
	hal_indicator_on();
	timer_stop(&(indicator.duration));
}

void indicator_off(void)
{
	hal_indicator_off();
	timer_stop(&(indicator.duration));
}

void indicator_blink(uint16_t on, uint16_t off)
{
	indicator.on = on;
	indicator.off = off;
	state_toggle();
}

void indicator_dispatch(void)
{
	timer_dispatch(&(indicator.duration));
}

void indicator_on_tick(void)
{
	timer_on_tick(&(indicator.duration));
}

void state_toggle(void)
{
	hal_indicator_toggle();
	uint16_t on_off_time = (false == hal_indicator_is_on()) ?
	                                indicator.off : indicator.on;
	timer_start(&(indicator.duration), on_off_time);
}

void duration_handler(__attribute__((unused)) void *arg)
{
	state_toggle();
}

