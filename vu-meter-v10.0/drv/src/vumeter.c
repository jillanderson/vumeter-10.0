#include "../vumeter.h"

#include <stddef.h>
#include <stdint.h>

#include "../../common/timer/timer.h"
#include "../../hal/input.h"
#include "../../hal/meter.h"

static struct {
	timer_st delay;
} vumeter = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

static void delay_handler(void *arg);

void vumeter_init(void)
{
	hal_input_init();
	hal_meter_init();
	timer_init(&(vumeter.delay));
	timer_handler(&(vumeter.delay), delay_handler, NULL);
}

void vumeter_on(void)
{
	/* zalecany czas debouncingu przekaźników miniaturowych wynosi 5-15ms */
	static uint8_t const relay_bounce_time_ms = 15U;

	hal_input_on();
	timer_start(&(vumeter.delay), relay_bounce_time_ms);
}

void vumeter_off(void)
{
	hal_meter_off();
	hal_input_off();
	timer_stop(&(vumeter.delay));
}

void vumeter_dispatch(void)
{
	timer_dispatch(&(vumeter.delay));
}

void vumeter_on_tick(void)
{
	timer_on_tick(&(vumeter.delay));
}

void delay_handler(__attribute__((unused)) void *arg)
{
	hal_meter_on();
}

