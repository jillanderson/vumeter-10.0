#include "../control.h"

#include <stdbool.h>
#include <stdint.h>

#include "../../common/filter/integrator.h"
#include "../../common/key/key.h"
#include "../../hal/key.h"

static struct {
	integrator_st debouncer;
	key_st key;
} control = { };
/* "{}" as an initializer can zero out the whole sizeof of the strcuture,
 including all kinds of padding. This is a GCC undocumented behavior */

void control_init(uint8_t debounce_time, uint16_t double_press_gap_time,
                  uint16_t long_press_start_time,
                  uint16_t long_press_repeat_time)
{
	hal_key_init();
	integrator_init(&(control.debouncer), debounce_time);
	key_init(&(control.key), double_press_gap_time, long_press_start_time,
	                long_press_repeat_time);
}

void control_handler(key_handler_ft *handler, void *arg)
{
	key_handler(&(control.key), handler, arg);
}

void control_dispatch(void)
{
	key_dispatch(&(control.key));
}

void control_on_tick(void)
{
	integrator_level_et key_level = (true == hal_key_is_pressed()) ?
	                                	INTEGRATOR_LEVEL_LOW :
	                                	INTEGRATOR_LEVEL_HIGH;
	integrator_on_tick(&(control.debouncer), key_level);
	bool key_is_pressed = (INTEGRATOR_LEVEL_LOW == integrator_level(&(control.debouncer))) ?
	                                true : false;
	key_on_tick(&(control.key), key_is_pressed);
}

