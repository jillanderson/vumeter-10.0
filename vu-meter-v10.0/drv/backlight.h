/*******************************************************************************
 *
 * obsługa podświetlania wskaźników wychyłowych
 *
 ******************************************************************************/

#pragma once

typedef enum {
	BACKLIGHT_NONE,
	BACKLIGHT_MINIMUM,
	BACKLIGHT_LOW,
	BACKLIGHT_MEDIUM,
	BACKLIGHT_HIGH,
	BACKLIGHT_MAXIMUM
} backlight_et;

/*
 * inicjuje sterowanie podświetlaniem wskaźników
 */
void backlight_init(void);

/*
 * ustawia jasność podświetlenia wskaźników
 */
void backlight_level(backlight_et level);

