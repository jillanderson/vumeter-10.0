/*******************************************************************************
 *
 * sterowanie wskaźnikiem optycznym w przycisku sterującym
 *
 ******************************************************************************/

#pragma once

#include <stdint.h>

/*
 * inicjalizacja - wskaźnik wyłączony
 */
void indicator_init(void);

/*
 * włącza wskaźnik
 */
void indicator_on(void);

/*
 * wyłącza wskaźnik
 */
void indicator_off(void);

/*
 * włącza tryb błyskania z zadanymi czasami włączenia i wyłączenia wskaźnika
 */
void indicator_blink(uint16_t on, uint16_t off);

/*
 * przetwarza zarejestrowane zadanie
 * musi być wywoływana w pętli głównej programu
 */
void indicator_dispatch(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void indicator_on_tick(void);

