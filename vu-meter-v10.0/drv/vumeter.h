/*******************************************************************************
 *
 * obsługa miernika vu (volume unit meter)
 *
 ******************************************************************************/

#pragma once

/*
 * inicjalizacja - miernik wyłączony
 */
void vumeter_init(void);

/*
 * włącza miernik
 */
void vumeter_on(void);

/*
 * wyłącza miernik
 */
void vumeter_off(void);

/*
 * przetwarza zarejestrowane zadanie
 * musi być wywoływana w pętli głównej programu
 */
void vumeter_dispatch(void);

/*
 * odlicza jednostki czasu
 * funkcja powinna być wywoływana cyklicznie, okres pomiędzy wywołaniami
 * tej funkcji definiuje jednostkę czasu
 */
void vumeter_on_tick(void);

